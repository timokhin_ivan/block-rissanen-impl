#ifndef FIELD_HPP__
#define FIELD_HPP__

#include <cassert>
#include <iostream>

#include <Eigen/Core>

namespace FieldInternal
{
  template <unsigned int N>
  struct Proxy {};
}

template <unsigned int P>
class ResidueField
{
private:
  unsigned int value;

  constexpr ResidueField<P> bruteforceInverse(unsigned int lower_bound = 0) const
  {
    return (ResidueField<P>(lower_bound) * *this == ResidueField<P>(1))
      ? ResidueField<P>(lower_bound)
      : (lower_bound >= P ? ResidueField<P>(0) : bruteforceInverse(lower_bound + 1));
  }

  template <unsigned int X>
  struct BruteforceInverse
  {
    enum { val = ResidueField<P>(X).bruteforceInverse().val() };
  };

  template <unsigned int LowerBound>
  constexpr ResidueField<P> lookupInverse(FieldInternal::Proxy<LowerBound>) const
  {
    return value == LowerBound
      ? ResidueField<P>(BruteforceInverse<LowerBound>::val)
      : lookupInverse(FieldInternal::Proxy<LowerBound + 1>());
  }

  constexpr ResidueField<P> lookupInverse(FieldInternal::Proxy<P>) const
  {
    return ResidueField<P>(0);
  }

public:
  constexpr explicit ResidueField(unsigned int v = 0) : value(v % P)
  {
  }

  constexpr explicit operator unsigned int () const
  {
    return value;
  }

  constexpr unsigned int val() const
  {
    return value;
  }

  constexpr ResidueField<P> inv() const
  {
    return lookupInverse(FieldInternal::Proxy<0>());
  }
};

template <unsigned int P>
constexpr ResidueField<P> operator + (ResidueField<P> x, ResidueField<P> y)
{
  return ResidueField<P>(x.val() + y.val());
}

template <unsigned int P>
constexpr ResidueField<P> operator - (ResidueField<P> x)
{
  return x.val() == 0 ? x : ResidueField<P>(P - x.val());
}

template <unsigned int P>
constexpr ResidueField<P> operator - (ResidueField<P> x, ResidueField<P> y)
{
  return x.val() > y.val()
    ? ResidueField<P>(x.val() - y.val())
    : ResidueField<P>(P + x.val() - y.val());
}

template <unsigned int P>
constexpr ResidueField<P> operator * (ResidueField<P> x, ResidueField<P> y)
{
  return ResidueField<P>(
    static_cast<unsigned int>(
      (static_cast<unsigned long long>(x.val()) *
       static_cast<unsigned long long>(y.val()))
      % P));
}

template <unsigned int P>
constexpr ResidueField<P> operator / (ResidueField<P> x, ResidueField<P> y)
{
  return x * y.inv();
}

template <unsigned int P>
constexpr bool operator == (ResidueField<P> x, ResidueField<P> y)
{
  return x.val() == y.val();
}

template <unsigned int P>
constexpr bool operator != (ResidueField<P> x, ResidueField<P> y)
{
  return !(x == y);
}

template <unsigned int P>
ResidueField<P>& operator += (ResidueField<P>& x, ResidueField<P> y)
{
  return x = x + y;
}

template <unsigned int P>
ResidueField<P>& operator -= (ResidueField<P>& x, ResidueField<P> y)
{
  return x = x - y;
}

template <unsigned int P>
ResidueField<P>& operator *= (ResidueField<P>& x, ResidueField<P> y)
{
  return x = x * y;
}

template <unsigned int P>
ResidueField<P>& operator /= (ResidueField<P>& x, ResidueField<P> y)
{
  return x = x / y;
}

template <unsigned int P>
std::ostream& operator << (std::ostream& os, ResidueField<P> x)
{
  return os << x.val();
}

namespace Eigen
{
  template <unsigned int P>
  struct NumTraits<::ResidueField<P>>
  {
    typedef ::ResidueField<P> Real;
    typedef ::ResidueField<P> NonInteger;
    typedef ::ResidueField<P> Nested;

    enum
    {
      IsComplex = 0,
      IsInteger = 1,
      ReadCost = 1,
      AddCost = 2,
      MulCost = 2,
      IsSigned = 0,
      RequireInitialization = 0
    };

    static inline Real epsilon()
    {
      return Real(1);
    }

    static inline Real dummy_precision()
    {
      return Real(0);
    }

    static inline Real highest()
    {
      return Real(P - 1);
    }

    static inline Real lowest()
    {
      return Real(0);
    }
  };
}

#endif // FIELD_HPP__
