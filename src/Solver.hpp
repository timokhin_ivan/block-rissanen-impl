#ifndef SOLVER_HPP__
#define SOLVER_HPP__

#include <algorithm>
#include <iterator>
#include <cassert>
#include <utility>
#include <stdexcept>
#include <iostream>

#include <Eigen/Core>

#ifndef SOLVER_LOG_LEVEL
#define SOLVER_LOG_LEVEL 0
#endif

#if SOLVER_LOG_LEVEL > 1
#define log(v) std::cout << (v) << std::endl
#else
#define log(v)
#endif

#if SOLVER_LOG_LEVEL > 0
#define log_callback(v) std::cout << (v) << std::endl
#else
#define log_callback(v)
#endif

namespace Rissanen {
  /*
  using arma::Col;
  using arma::Row;
  using arma::Mat; */

  template <typename T>
  using Mat = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  template <typename T>
  using Row = Eigen::Matrix<T, 1, Eigen::Dynamic, Eigen::RowMajor>;


  namespace Block {
    template <typename T>
    class Hankel {
    private:
      Mat<T> elements_;
      size_t rows_, columns_;
      size_t blockRows_, blockColumns_;
    public:
      Hankel() : elements_(), rows_(0), columns_(0), blockRows_(0), blockColumns_(0)
      {}

      template <typename Els>
      Hankel(Els&& elements,
	     size_t rows, size_t columns,
	     size_t blockRows, size_t blockColumns)
	: elements_(std::forward<Els>(elements))
	, rows_(rows)
	, columns_(columns)
	, blockRows_(blockRows)
	, blockColumns_(blockColumns)
      {
	if (blockRows_ != elements_.outerSize())
	  throw std::logic_error("Block size mismatch in block Hankel constructor");

	if ((rows_ + columns_ - 1) * blockColumns_ != elements_.innerSize())
	  throw std::logic_error("Size mismatch in block Hankel constructor");
      }

      const Mat<T>& elements() const
      {
	return elements_;
      }

      size_t rows() const
      {
	return rows_;
      }

      size_t columns() const
      {
	return columns_;
      }

      size_t blockRows() const
      {
	return blockRows_;
      }

      size_t blockColumns() const
      {
	return blockColumns_;
      }

      Mat<T> matmul(const Mat<T> & b) const
      {
	assert(b.outerSize() == columns_ * blockColumns_);

	Mat<T> result = Mat<T>(rows_ * blockRows_, b.innerSize()).setZero();

	for (size_t i = 0; i < rows_; ++i)
	{
	  result.middleRows(i * blockRows_, blockRows_)
	    = elements_.middleCols(i * blockColumns_, columns_ * blockColumns_)
	    * b;
	}

	return result;
      }
    };

    template <typename T>
    Mat<T> solveHankel(const Hankel<T> & matrix, Mat<T> rhs)
    {
      size_t block_row = 0;
      size_t n = matrix.columns();
      size_t s = matrix.blockColumns();
      size_t m = rhs.innerSize();

      auto y = Mat<T>(n*s, m).setZero(),
	   x = Mat<T>(n*s, m).setZero();

      walkPQDecomposition(matrix,
			  [&block_row, &y, &x, &rhs, n, s, m, &matrix]
			  (auto&& p, auto&&q)
      {
	log_callback("In callback:\nP:");
	log_callback(p);
	log_callback("Q");
	log_callback(q);
	log_callback("block_row");
	log_callback(block_row);
	assert(matrix.matmul(p.transpose()) == q.transpose());

	// Solve for y
	for (size_t j = 0; j < s; ++j)
	{
	  log_callback("y");
	  log_callback(y.transpose());
	  log_callback("rhs");
	  log_callback(rhs.transpose());

	  auto row = block_row * s + j;
	  y.row(row) = rhs.row(row) / q(j, row);

	  rhs -= q.row(j).transpose() * y.row(row);

	  log_callback("After adjustment");
	  log_callback("y");
	  log_callback(y.transpose());
	  log_callback("rhs");
	  log_callback(rhs.transpose());

	  x += p.row(j).transpose() * y.row(row);
	  log_callback("x");
	  log_callback(x.transpose());
	}

	++block_row;
      });

      return x;
    }

    // l is used only for output; if it is A on entry, it will be PLA
    // on exit
    // u is used for input *and* output
    // n.b. l and u are not necessarily square matrices.  They have to
    // have the same number of rows, however
    // pass by value because used on subviews
    template <typename T, typename MatL, typename MatU>
    bool lu(MatL l, MatU u, bool reverse = false)
    {
      size_t nrows = l.outerSize();
      size_t ncols = u.innerSize();
      assert(nrows == u.outerSize());

      for (size_t i = 0; i < nrows && i < ncols; ++i)
      {
	size_t col = reverse ? ncols - i - 1 : i;
	size_t row = 0;

	for (row = i; row < nrows; ++row)
	  if (u(row, col) != T(0))
	    break;

	if (row >= nrows)
	  return false;

	u.row(i).swap(u.row(row));
	l.row(i).swap(l.row(row));

	for (size_t j = i + 1; j < nrows; ++j)
	{
	  auto coef = u(j, col) / u(i, col);

	  u.row(j) -= coef * u.row(i);
	  l.row(j) -= coef * l.row(i);
	}
      }

      return true;
    }

    // qExc is assumed to be upper triangular, which just happens to
    // be the case in the algorithm
    template <typename MatQE, typename MatPE, typename MatQT, typename MatPT>
    void exclude(MatQE qExc, MatPE pExc, MatQT qTarget, MatPT pTarget)
    {
      log("Inside \"exclude\"");
      log("excluded");
      log("q");
      log(qExc);
      log("p");
      log(pExc);
      log("target");
      log("q");
      log(qTarget);
      log("p");
      log(pTarget);

      size_t nrows = qExc.outerSize();
      assert(pExc.outerSize() == qTarget.outerSize()
	     && qTarget.outerSize() == pTarget.outerSize()
	     && pTarget.outerSize() == nrows);

      for (size_t i = 0; i < nrows; ++i)
      {
	for (size_t j = 0; j < nrows; ++j)
	{
	  auto coef = qTarget(j, i) / qExc(i, i);
	  qTarget.row(j) -= coef * qExc.row(i);
	  pTarget.row(j) -= coef * pExc.row(i);
	}
      }

      log("Exiting \"exclude\"");
      log("q");
      log(qTarget);
      log("p");
      log(pTarget);
    }

    template <typename T, typename Callback>
    void walkPQDecomposition(const Hankel<T>& matrix, const Callback& callback)
    {
      log("Matrix:");
      log(matrix.elements());

      auto n = matrix.columns();
      auto s = matrix.blockColumns();

      assert(n == matrix.rows());
      assert(s == matrix.blockRows());

      auto pRows = Mat<T>(4*s, n*s).setZero();
      auto qRows = Mat<T>(4*s, n*s).setZero();
      auto qtemp = Mat<T>(2*s, n*s).setZero(); // Needed only
				           	// for
				           	// convenience
      auto ptemp = Mat<T>(2*s, n*s).setZero();

      for (size_t i = 0; i < n; ++i)
      {
	qRows.block(0, s*i, s, s) =
	  matrix.elements().middleCols(s*i, s).transpose();
	qRows.block(s, s*i, s, s) =
	  matrix.elements().middleCols(s*(i + 1), s).transpose();
      }

      pRows.block(0, 0, 2*s, 2*s).setIdentity(2*s, 2*s);

      lu<T>(pRows.middleRows(0, 2*s), qRows.middleRows(0, 2*s));
      log("After initial LU");
      log("p");
      log(pRows);
      log("q");
      log(qRows);

      assert(matrix.matmul(pRows.transpose()) == qRows.transpose());
      callback(pRows.middleRows(0, s), qRows.middleRows(0, s));

      qtemp = qRows.middleRows(0, 2*s);
      ptemp = pRows.middleRows(0, 2*s);
      lu<T>(qtemp.middleRows(0, 2*s), ptemp.block(0, 0, 2*s, 2*s), true);
      log("qtemp");
      log(qtemp);
      log("ptemp");
      log(ptemp);
      pRows.block(2*s, s, s, 2*s) = ptemp.block(0, 0, s, 2*s);
      qRows.block(2*s, 0, s, n*s - s) = qtemp.block(0, s, s, n*s-s);

      qRows.block(2*s, n*s-s, s, s) =
	pRows.middleRows(2*s, s) *
	matrix.elements().middleCols((n - 1)*s, n*s).transpose();

      log("After processing first two rows");
      log("p");
      log(pRows);
      log("q");
      log(qRows);
            assert(matrix.matmul(pRows.transpose()) == qRows.transpose());

      // Rows are counted from zero
      for (size_t row = 2; row < n; ++row)
      {
	log("Row:");
	log(row);
	size_t idx[4] = {row % 4, (row + 3) % 4, (row + 2) % 4, (row + 1) % 4};

	if (row > 2)
	{
	  exclude(qRows.block(idx[3] * s, (row - 3) * s, s, (n - row + 3)*s),
		  pRows.middleRows(idx[3] * s, s),
		  qRows.block(idx[0] * s, (row - 3) * s, s, (n - row + 3)*s),
		  pRows.middleRows(idx[0] * s, s));
	  log("After excluding pre-pre-previous row");
	  log("p");
	  log(pRows);
	  log("q");
	  log(qRows);
	        assert(matrix.matmul(pRows.transpose()) == qRows.transpose());
	}

	exclude(qRows.block(idx[2] * s, (row - 2) * s, s, (n - row + 2)*s),
		pRows.middleRows(idx[2] * s, s),
		qRows.block(idx[0] * s, (row - 2) * s, s, (n - row + 2)*s),
		pRows.middleRows(idx[0] * s, s));

	log("After excluding pre-previous row");
	log("p");
	log(pRows);
	log("q");
	log(qRows);
	assert(matrix.matmul(pRows.transpose()) == qRows.transpose());

	if (row < n - 1)
	{
	  qRows.block(idx[3]*s, 0, s, n*s - s) =
	    qRows.block(idx[0]*s, s, s, n*s - s);
	  pRows.block(idx[3]*s, s, s, n*s - s) =
	    pRows.block(idx[0]*s, 0, s, n*s - s);
	  pRows.block(idx[3]*s, 0, s, s).setZero();

	  qRows.block(idx[3]*s, n*s - s, s, s) =
	    pRows.middleRows(idx[3]*s, s) *
	    matrix.elements().middleCols((n - 1) * s, n*s).transpose();

	  log("After shifting to make the next row template");
	  log("p");
	  log(pRows);
	  log("q");
	  log(qRows);
	  assert(matrix.matmul(pRows.transpose()) == qRows.transpose());
	}

	qtemp.middleRows(0, s) = qRows.middleRows(idx[1]*s, s);
	ptemp.middleRows(0, s) = pRows.middleRows(idx[1]*s, s);
	qtemp.middleRows(s, s) = qRows.middleRows(idx[0]*s, s);
	ptemp.middleRows(s, s) = pRows.middleRows(idx[0]*s, s);

	log("Before combining rows");
	log("q");
	log(qtemp);
	log("p");
	log(ptemp);
	assert(matrix.matmul(pRows.transpose()) == qRows.transpose());

	lu<T>(ptemp.middleRows(0, 2*s), qtemp.block(0, (row - 1)*s, 2*s, (n - row + 1)*s));

	log("After combining rows");
	log("q");
	log(qtemp);
	log("p");
	log(ptemp);
	      assert(matrix.matmul(pRows.transpose()) == qRows.transpose());

	qRows.middleRows(idx[1]*s, s) = qtemp.middleRows(0, s);
	qRows.middleRows(idx[0]*s, s) = qtemp.middleRows(s, s);
	pRows.middleRows(idx[1]*s, s) = ptemp.middleRows(0, s);
	pRows.middleRows(idx[0]*s, s) = ptemp.middleRows(s, s);

	log("After combining current and previous rows");
	log("p");
	log(pRows);
	log("q");
	log(qRows);
	      assert(matrix.matmul(pRows.transpose()) == qRows.transpose());

	callback(pRows.middleRows(idx[1]*s, s),
		 qRows.middleRows(idx[1]*s, s));
      }

      size_t last = (n - 1) % 4;

      assert(matrix.matmul(pRows.transpose()) == qRows.transpose());
      log("Prior to final callback");
      log(last);
      log("p");
      log(pRows);
      log("q");
      log(qRows);

      callback(pRows.middleRows(last*s, s),
	       qRows.middleRows(last*s, s));
    }
  }
}

#endif // SOLVER_HPP__
