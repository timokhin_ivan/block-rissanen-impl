#include <iostream>
#include <fstream>
#include <chrono>
#include <random>
#define SOLVER_LOG_LEVEL 0
#include "Solver.hpp"
#include "Field.hpp"

template <unsigned int P>
Rissanen::Mat<ResidueField<P>> makeRandomMatrix(size_t rows, size_t cols)
{
  Rissanen::Mat<ResidueField<P>> result {rows, cols};

  std::random_device r;
  std::mt19937 rnd(r());
  std::uniform_int_distribution<unsigned> dist(0, P - 1);

  for (auto x = result.data();
       x < result.data() + result.outerSize() * result.innerSize(); ++x)
  {
    *x = ResidueField<P>(dist(rnd));
  }

  return result;
}

template <unsigned int P>
Rissanen::Block::Hankel<ResidueField<P>> makeRandomBHMatrix
  (size_t rows,
   size_t cols,
   size_t brows,
   size_t bcols)
{
  return Rissanen::Block::Hankel<ResidueField<P>>
    (makeRandomMatrix<P> (brows, (rows + cols - 1) * bcols),
     rows, cols, brows, bcols);
}

template <unsigned int P>
double timeSolver(size_t size, size_t blockSize, size_t rhss = 1, size_t runs = 3)
{
  std::chrono::duration<double> totalSeconds {0.0};

  for (size_t i = 0; i < runs; ++i)
  {
    auto A = makeRandomBHMatrix<P>(size, size, blockSize, blockSize);
    auto b = makeRandomMatrix<P>(size * blockSize, rhss);

    auto start = std::chrono::high_resolution_clock::now();
    auto x = Rissanen::Block::solveHankel(A, b);
    auto end = std::chrono::high_resolution_clock::now();

    totalSeconds += end - start;
  }

  return totalSeconds.count() / runs;
}

Rissanen::Mat<double> makeRandomRealMatrix(size_t rows, size_t cols)
{
  Rissanen::Mat<double> result {rows, cols};

  std::random_device r;
  std::mt19937 rnd(r());
  std::normal_distribution<> dist;

  for (auto x = result.data();
       x < result.data() + result.outerSize() * result.innerSize(); ++x)
  {
    *x = dist(rnd);
  }

  return result;
}

Rissanen::Block::Hankel<double> makeRandomBHRealMatrix
  (size_t rows,
   size_t cols,
   size_t brows,
   size_t bcols)
{
  return Rissanen::Block::Hankel<double>
    (makeRandomRealMatrix(brows, (rows + cols - 1) * bcols),
     rows, cols, brows, bcols);
}

double measureError(size_t size, size_t bsize, size_t runs = 3)
{
  double err = 0.0;

  for (size_t i = 0; i < runs; ++i)
  {
    auto A = makeRandomBHRealMatrix(size, size, bsize, bsize);
    auto b = makeRandomRealMatrix(size*bsize, 1);
    auto x = Rissanen::Block::solveHankel(A, b);

    err += (A.matmul(x) - b).norm();
  }

  return err / runs;
}

int main()
{
  if (0)
  {
    {
      std::ofstream out {"varn.data"};

      for (size_t size = 5; size < 200; size += 5)
	out << size << '\t' << timeSolver<13>(size, 10) << std::endl;

      std::cout << "Varying n done" << std::endl;
    }

    {
      std::ofstream out {"vark.data"};

      for (size_t bsize = 4; bsize < 50; bsize += 2)
	out << bsize << '\t' << timeSolver<13>(20, bsize) << std::endl;

      std::cout << "Varying k done" << std::endl;
    }

    {
      std::ofstream out {"varrhs.data"};

      for (size_t nrhs = 1; nrhs < 50; ++nrhs)
	out << nrhs << '\t' << timeSolver<13>(50, 20, nrhs) << std::endl;

      std::cout << "Varying nrhs done" << std::endl;
    }
  }

  if (1)
  {
    std::ofstream out {"varnerr.data"};

    for (size_t size = 5; size < 200; size += 5)
      out << size << '\t' << measureError(size, 10) << std::endl;

    std::cout << "Measuring error with varying n done" << std::endl;
  }

  return 0;
}
